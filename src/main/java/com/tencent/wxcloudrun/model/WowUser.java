package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class WowUser implements Serializable {

  private String tel;
  private String avator;
  private String nickName;
  private String vipOverdue;
  private String createTime;
  private String updateTime;
  private String chargeStatus;
  private String openId;
  private String msgCount;
  private String isAcceptMsg;
  private String isVip;
  private String reqStatus;
}
