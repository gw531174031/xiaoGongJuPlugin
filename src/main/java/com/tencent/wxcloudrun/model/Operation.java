package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Operation implements Serializable {

  private String id;

  private String tel; //操作人手机号

  private String operator; //操作人 姓名

  private String operation;

  private Long createTime;

  private String type; // 1: 订单数据操作 2： 商品数据操作，3：账号数据操作

  private String linkId;//关联的外键ID

  public Operation(){

  }

  public Operation(String tel, String operation, String linkId, String type, Long createTime ) {
    this.tel =tel;
    this.operation = operation;
    this.linkId = linkId;
    this.type = type;
    this.createTime = createTime;
  }

}
