package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class User implements Serializable {

  private Integer id;

  private String tel;

  private String token;

  private String wxOpenid;

  private String avatar;

  private String nickname;

  private Long vipStartTime;

  private Long vipEndTime;

  private Long createTime;

  private Long updateTime;

  private String qrUrl;  //会员二维码的图片地址

  private List<OrdersStatis> ordersStatisList; //用户订单 已下单 配送中  借还物品的统计数据

  private String isVip;

  private String QRFileName;

  private String loginCode; //用于获取用户的openId

  private String area; //账户户的所在区域

  private String distributionStation; //账户的所在配送中

  private String estate; //账户所在的小区

  private String areaName; //账户户的所在区域

  private String dsName; //账户的所在配送中

  private String estateName; //账户所在的小区

  private Long lastSignTime; //账户最后一次登录时间

  private String  actived; //账户是否启用

  private String deleted; // 账户是否删除

  private String password; //账户的密码

  private List<Role> roleList; //账户的角色信息

  private String remark;

  private String accountType; //账户类型 1：员工  2： 用户


}
