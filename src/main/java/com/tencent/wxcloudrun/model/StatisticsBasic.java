package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class StatisticsBasic implements Serializable {

  private Integer todayOrderNum; //今日订单数
  private Float todayOrderAmount; //今日销售额
  private Float yesterdayOrderNum; //昨日销售额
  private Float todayPurchaseAmount; //今日采购额
  private Float yesterdayPurchaseAmount; //昨日采购额
  private Integer currentUserNum; //当前用户数

  private Integer statusOrderedNum; //订单状态 已下单 的数量
  private Integer statusSortedNum; //订单状态 已分拣 的数量
  private Integer statusFinishNum; //订单状态 已完成 的数量
  private Integer statusCancelNum; //订单状态 已取消 的数量
  private Integer statusAfterSaleNum; //订单状态 已申请售后 的数量

  private Integer todayActiveUserNum; //今日活跃用户
  private Integer yesterdayActiveUserNum; //昨日活跃用户
  private Integer weekActiveUserNum; //本周活跃用户
  private Integer monthActiveUserNum; //本月活跃用户

  private Integer lastWeekActiveUserNum; //上周活跃用户
  private Integer lastMonthActiveUserNum; //上月活跃用户

  private Float weekActiveUserNumRatio; //同比上周活跃用户比率
  private Float monthActiveUserNumRatio; //同比上周活跃用户比率

  private Integer todayNewlyUserNum; //今日新增用户
  private Integer weekNewlyUserNum; //本周新增用户
  private Integer monthNewlyUserNum; //本月用户
  private Integer totalVipUserNum; //当前会员总数

  private Integer weekOrderNum; //本周订单数
  private Integer lastWeekOrderNum; //本周订单数
  private Integer monthOrderNum; //本月订单数
  private Integer lastMonthOrderNum; //本月订单数
  private Float weekOrderNumRatio; //同比上周订单数比率
  private Float monthOrderNumRatio; //同比上月订单数比率


  private Map<String, StatisticsModel> chartDataMap;  //图表的数据


}
