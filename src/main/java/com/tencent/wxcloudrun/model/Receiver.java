package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Receiver implements Serializable {

  private String id;

  private String tel;

  private String receiverName;

  private String receiverTel;

  private String province;

  private String city;
  private String district;

  private String addrDetail;

  private Long createTime;

  private Long updateTime;

  private String estate; // 收件人小区

  private String estateName; // 收件人小区名称

}
