package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatisticsModel implements Serializable {

  private String key1;

  private Integer value1; //

  private Integer value2; //

}
