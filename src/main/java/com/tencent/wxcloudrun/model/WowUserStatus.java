package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class WowUserStatus implements Serializable {

  private String tel;
  private String snapPicture;
  private String snapTime;
  private String isOnline;
  private String updateTime;
  private String isVip;
  private String userCount;
  private String chargeStatus;
  private List<String> avatorList;
  private String msgCount; //剩余消息通知次数
  private String reqStatus;
  private String isAcceptMsg;
}
