package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Role implements Serializable {

  private String id;

  private String roleName;

  private String roleDesc;

  private String actived;

  private String deleted;

  private Long createTime;

  private Long updateTime;
}
