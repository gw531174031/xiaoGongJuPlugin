package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Orders implements Serializable {

  private String id;

  private String tel;

  private String nickname;

  private String status;

  private String receiveTime;

  private String receiverId;

  private String receiverName;

  private String receiverTel;

  private String province;

  private String city;

  private String district;

  private String area;

  private String distributionStation;

  private String estate;

  private String addrDetail;

  private String remark;

  private Long createTime;

  private Long updateTime;

  private List<OrdersGoods> goodsList;

  private Float orderAmount;  //下单时总金额

  private Float deliverAmount; //发货时总金额

  private Integer totalGoodsNum;

  private List<Operation> orderOperationList;

  private String serviceReason;

  private String serviceImg;

  private String serviceType;


}
