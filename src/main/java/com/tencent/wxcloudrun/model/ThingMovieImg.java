package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ThingMovieImg implements Serializable {

  private String id;

  private String thingMovieId;

  private String url;

  private String orderNum;

  private String type;
  
  private Long createTime;

  private Long updateTime;
}
