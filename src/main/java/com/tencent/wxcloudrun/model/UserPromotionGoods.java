package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserPromotionGoods implements Serializable {

  private String tel;

  private String goodsId;


}
