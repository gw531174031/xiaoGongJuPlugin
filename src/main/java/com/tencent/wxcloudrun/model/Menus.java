package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Menus implements Serializable {

  private String id;

  private String name;

  private String title;

  private String level;

  private String parentId;

  private String icon;

  private String sort;

  private String hidden;

  private String deleted;

  private Long createTime;

}
