package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserThing implements Serializable {

  private String tel;

  private String nickname;

  private Integer thingNum;

  private String status;

  private Long createTime;

  private String thingId;

  private String title;

  private Integer totalNum;

  private String type;

  private String isVipOnly;

}
