package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OrdersGoods implements Serializable {

  private String id;

  private String orderId;

  private String title;

  private String outOfSale;
  private String tags;

  private String calType;

  private String thumb;

  private String isSale;

  private String desc;

  private String status; //订单商品的状态，1：首次添加，2：取消商品，3：修改重量，4：申请售后

  private Long createTime;

  private String[] tagArray;

  private String isSelected;  //给购物车里面使用的

  private String priceType; //订单中商品的计价类型 1： vip  0：市场价

  private String goodsClassType; //商品的类型  1：蔬菜 2：水果

  // 这是给单个订单使用的
  private Float orderVipPrice;
  private Float orderOriginPrice;
  private Integer orderGoodsNum;
  private Float deliverVipPrice;
  private Integer deliverGoodsNum;

  //这是给后台统计使用的
  private Integer orderGoodsTotalNum; //下单的总数量
  private Integer firstPurchaseNum; //第一次采购数量
  private Integer secondPurchaseNum; //第二次采购数量
  private Integer returnGoodsNum; //退还数量，这里在某些情况下 如果单节点采购过多，可以退还总部
  private Float lastWholesalePrice; //昨日批发价
  private Float wholesalePrice; //今日批发价
  private Float vipPrice; //上架价
  private String purchaseGoodsPriceId; //商品采购价格id
  private String purchaseGoodsId; //商品采购id
  private String area; //区域
  private String distributionStation; //配送站
  private String purchaseDate; //采购日期


}
