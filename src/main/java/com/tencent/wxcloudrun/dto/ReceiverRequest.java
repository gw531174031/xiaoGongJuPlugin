package com.tencent.wxcloudrun.dto;

import lombok.Data;

@Data
public class ReceiverRequest {

  private String id;

  private String tel;

  private String receiverName;

  private String receiverTel;

  private String province;

  private String city;

  private String district;

  private String addrDetail;

  private String estate; //小区

}
