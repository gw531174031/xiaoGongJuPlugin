package com.tencent.wxcloudrun.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatisticsRequest implements Serializable {

  private String tel;

  private String areaId; //区域ID

  private String dsId; // 配送站Id

  private String estateId; // 小区Id

  private String goodsClassType;

  private String goodsId; //商品Id

  private Long statisticsStartTime;

  private Long statisticsEndTime;

}
