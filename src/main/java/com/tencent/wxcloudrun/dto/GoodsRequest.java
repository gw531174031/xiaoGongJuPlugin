package com.tencent.wxcloudrun.dto;

import lombok.Data;

@Data
public class GoodsRequest {

  private String id;

  private String outOfSale;  //商品上架 下架 状态

  private String hotSale;

  private String isDelete;

  private int currentPage;

  private int pageSize;

  private String title;

  private String goodsClassType; //商品类型

  private String desc;

  private String tags;

  private Float vipPrice;

  private Float originPrice;

  private String calType;

  private Integer floorBuyNum;// 起购数量

  private Integer topBuyNum; //最高限购多少件

  private String promotion; //是否促销

  private Integer stock; //促销商品数量

  private String goodsImgs; //图片照片

  private String thumb; //小图

  private Integer orderNum; //排序的字段

  private String areaId; //区域ID

  private String dsId; //配送站ID

  //这是给后台统计使用的
  private Integer orderGoodsTotalNum; //下单的总数量
  private Integer firstPurchaseNum; //第一次采购数量
  private Integer secondPurchaseNum; //第二次采购数量
  private Integer returnGoodsNum; //退还数量，这里在某些情况下 如果单节点采购过多，可以退还总部
  private Float lastWholesalePrice; //昨日批发价
  private Float wholesalePrice; //今日批发价
  private String purchaseGoodsPriceId; //商品采购价格id
  private String purchaseGoodsId; //商品采购id
  private String purchaseDate; //采购日期

}
