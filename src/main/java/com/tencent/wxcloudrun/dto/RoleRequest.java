package com.tencent.wxcloudrun.dto;

import lombok.Data;

@Data
public class RoleRequest {

  private String id;

  private String roleName;

  private Integer currentPage;

  private Integer pageSize;

}
