package com.tencent.wxcloudrun.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonAreaRequest implements Serializable {

  private String id;

  private String name;

  private String parentId;

  private String areaType; // 区域类型 1：area  2： 配送站  3：小区

}
