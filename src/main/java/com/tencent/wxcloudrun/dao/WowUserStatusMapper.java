package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WowUserStatus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WowUserStatusMapper {

  WowUserStatus getUserStatus(@Param("tel") String tel);

  void updateUserStatus(WowUserStatus userStatus);

  void insertUserStatus(WowUserStatus userStatus);
}
