package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.Menus;
import com.tencent.wxcloudrun.model.Receiver;
import com.tencent.wxcloudrun.model.Role;
import com.tencent.wxcloudrun.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {

  User getUserByTel(@Param("tel") String tel);

  void insertUser( User user );

  void updateUserVipTime( User user );

  void updateUserForQR(User user);

  List<Menus> getMenusByTel(@Param("tel") String tel);

  List<User> getUserByConditions(Map<String, Object> paramMap);

  Integer getUserTotalNumByConditions(Map<String, Object> paramMap);

  List<Role> getRolesByConditions(Map<String, Object> paramMap);

  void deleteRoleUserByTel(@Param("tel") String tel);

  void patchInsertRoleUser(Map<String, Object> paramMap);

  void updateUserStatus(Map<String, Object> paramMap);

  void updateUserBasicInfo(User user);
}
