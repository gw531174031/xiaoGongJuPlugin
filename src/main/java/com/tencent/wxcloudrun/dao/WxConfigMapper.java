package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.WxConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WxConfigMapper {

    void clearToken();

    void insertToken(WxConfig wxConfig);

    WxConfig getToken();

}