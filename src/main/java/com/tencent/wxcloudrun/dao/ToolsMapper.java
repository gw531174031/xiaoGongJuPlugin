package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.Operation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolsMapper {

  List<Dictionary> getDictionaryListByType(@Param("type") String type);



}
