package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.CommonArea;
import com.tencent.wxcloudrun.model.Receiver;
import com.tencent.wxcloudrun.model.StatisticsBasic;
import com.tencent.wxcloudrun.model.StatisticsModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface StatisticsMapper {


  StatisticsBasic getOrderBasicStatistics(Map<String, Object> paramMap);

  StatisticsBasic getGoodsPurchaseStatistics(Map<String, Object> paramMap);

  StatisticsBasic getOrderStatusStatistics(Map<String, Object> paramMap);

  StatisticsBasic getUserStatistics(Map<String, Object> paramMap);

  List<StatisticsModel> getOrderStatistics(Map<String, Object> paramMap);

  List<StatisticsModel> getUserActiveStatistics(Map<String, Object> paramMap);

  List<StatisticsModel> getGoodsClassStatistics(Map<String, Object> paramMap);

  List<StatisticsModel> getGoodsStatistics(Map<String, Object> paramMap);


  
}
