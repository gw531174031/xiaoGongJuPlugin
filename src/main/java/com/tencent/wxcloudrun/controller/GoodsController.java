package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.dto.GoodsCartRequest;
import com.tencent.wxcloudrun.dto.GoodsRequest;
import com.tencent.wxcloudrun.dto.UserRequest;
import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.GoodsImg;
import com.tencent.wxcloudrun.model.OrdersGoods;
import com.tencent.wxcloudrun.service.CommonToolsService;
import com.tencent.wxcloudrun.service.GoodsService;
import com.tencent.wxcloudrun.service.impl.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * counter控制器
 */
@RestController
public class GoodsController {

  final GoodsService goodsService;
  final Logger logger;

  final FileService fileService;

  final CommonToolsService commonToolsService;

  public GoodsController(GoodsService goodsService, FileService fileService, CommonToolsService commonToolsService) {
    this.goodsService = goodsService;
    this.fileService = fileService;
    this.commonToolsService = commonToolsService;

    this.logger = LoggerFactory.getLogger(GoodsController.class);
  }

  /**
   * APP按条件取商品列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/goods/list")
  ApiResponse getGoodsListForApp(@RequestBody GoodsRequest request) {
    logger.info("Request APP按条件取商品列表信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("goodsClassType", request.getGoodsClassType() != null ? request.getGoodsClassType() : null);
      paramMap.put("hotSale", request.getHotSale() != null ? request.getHotSale() : null);
      paramMap.put("currentIndex", (request.getCurrentPage() - 1) * request.getPageSize());
      paramMap.put("pageSize", request.getPageSize());

      List<Goods> goodsList = goodsService.getGoodsPageByConditions(paramMap);
      for (Goods g: goodsList ) {
        //处理标签问题
        if(g.getTags() != null || "".equals(g.getTags())) {
          g.setTagArray(g.getTags().split(",")); //多个标签使用逗号分割
        } else {
          g.setTagArray(null);
        }
      }
      Map<String, Object> data = new HashMap<>();
      data.put("list", goodsList);

      //查询字典
      List<Dictionary> goodsClassList = commonToolsService.getDictionaryListByType("goodsClass");
      data.put("goodsClass", goodsClassList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 APP按条件取商品列表信息，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }


  /**
   * 按类型获取商品列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/goods/list")
  ApiResponse getGoodsList(@RequestBody GoodsRequest request) {
    logger.info("Request 根据类型获取商品列表信息, 参数：" + JSONObject.toJSONString(request));

    try {

      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("title", request.getTitle() != null ? request.getTitle() : null);
      paramMap.put("goodsClassType", request.getGoodsClassType() != null ? request.getGoodsClassType() : null);
      paramMap.put("hotSale", request.getHotSale() != null ? request.getHotSale() : null);
      paramMap.put("outOfSale", request.getOutOfSale() != null ? request.getOutOfSale() : null);
      paramMap.put("id", request.getId() != null ? request.getId() : null);
      paramMap.put("promotion", request.getPromotion() != null ? request.getPromotion() : null);
      paramMap.put("currentIndex", (request.getCurrentPage() - 1) * request.getPageSize());
      paramMap.put("pageSize", request.getPageSize());

      List<Goods> goodsList = goodsService.getGoodsPageByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("list", goodsList);
      Integer totalNum = goodsService.getGoodsTotalNumByConditions(paramMap);
      data.put("total",  totalNum != null ?  totalNum : 0);

      //查询字典
      List<Dictionary> goodsClassList = commonToolsService.getDictionaryListByType("goodsClass");
      data.put("goodsClass", goodsClassList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取商品列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据手机号获取购物车列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/goods/getGoodsCart")
  ApiResponse getGoodsCart(@RequestBody UserRequest request) {
    logger.info("Request 根据手机号获取购物车列表信息, 参数："+ JSONObject.toJSONString(request));

    try {
      List<Goods> goodsList = goodsService.getGoodsCartByTel(request.getTel());
//      logger.info("返回的数据："+ JSONObject.toJSONString(goodsList));
      Map<String, Object> data = new HashMap<>();
      data.put("list", goodsList);


      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据手机号获取购物车信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据商品id获取商品详细信息  APP
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/goods/getGoodsDetail")
  ApiResponse getAppGoodsDetail(@RequestBody GoodsRequest request) {
    logger.info("Request 根据商品id获取商品详细信息 APP, 参数：" + JSONObject.toJSONString(request));

    try {
      Goods goods = goodsService.getGoodsById(request.getId());
      //处理标签问题
      if(goods.getTags() != null || "".equals(goods.getTags())) {
        goods.setTagArray(goods.getTags().split(",")); //多个标签使用逗号分割
      } else {
        goods.setTagArray(null);
      }
      Map<String, Object> data = new HashMap<>();
      data.put("data", goods);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据商品id获取商品详细信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据商品id获取商品详细信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/getGoodsDetail")
  ApiResponse getGoodsDetail(@RequestBody GoodsRequest request) {
    logger.info("Request 根据商品id获取商品详细信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Goods goods = goodsService.getGoodsById(request.getId());
      return ApiResponse.ok(goods);
    } catch (Exception e) {
      logger.error("controller 异常 根据商品id获取商品详细信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新用户购物车商品信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/goods/updateGoodsCart")
  ApiResponse updateGoodsCart(@RequestBody GoodsCartRequest request) {
    logger.info("Request 更新购物车商品信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Long nowTime = (new Date()).getTime();
      Goods goods = new Goods();
      goods.setId(request.getGoodsId());
      goods.setGoodsNum(request.getGoodsNum());
      goods.setCreateTime(nowTime);
      goods.setUpdateTime(nowTime);

      goodsService.insertGoodsCart(request.getTel(), goods);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 插入购物车商品信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 删除用户购物车商品信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/goods/deleteGoodsCart")
  ApiResponse deleteGoodsCart(@RequestBody GoodsCartRequest request) {
    logger.info("Request 删除购物车商品信息, 参数：" + JSONObject.toJSONString(request));

    try {
      goodsService.deleteCartByGoodsId(request.getTel(), request.getGoodsId());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 删除购物车商品信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /*
   * 图片文件上传
   */
  @PostMapping("/goods/img/upload")
  public ApiResponse UploadPicture(@RequestParam("file") MultipartFile file, @RequestParam("id") String goodsId) throws IOException {

    logger.info("save file name {}, goodsId {}", file.getOriginalFilename(), goodsId);

    try {
      Map<String, Object> data = new HashMap<>();
      //相对路径需要增加/apis/file 以便页面可以访问
      String fileName = "/apis/file/" + fileService.saveFile(file, goodsId);
      data.put("data", fileName);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 上传图片信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 删除图片
   * @return
   * @throws IOException
   */
  @PostMapping("/goods/img/delete")
  public ApiResponse deletePicture(@RequestBody GoodsRequest request) throws IOException {

    logger.info("delete file name {},", request.getGoodsImgs());

    try {
      Map<String, Object> data = new HashMap<>();
      //获取图片的文件路径
      String fileName = request.getGoodsImgs();
      if(fileName.startsWith("/apis/file/")) {
        fileName = fileName.substring(10, fileName.length()-1);
      }
      data.put("data", fileService.deleteFile(fileName));
      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 删除图片信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 添加商品
   * @return
   * @throws IOException
   */
  @PostMapping("/goods/create")
  public ApiResponse insertGoods(@RequestBody GoodsRequest request) throws IOException {

    logger.info("添加商品接口参数 {},", request);

    try {
      Goods goods = new Goods();
      goods.setId(request.getId());
      goods.setTitle(request.getTitle());
      goods.setVipPrice(request.getVipPrice());
      goods.setOriginPrice(request.getOriginPrice());
      goods.setTags(request.getTags());
      goods.setCalType(request.getCalType());
      goods.setThumb(request.getThumb());
      goods.setDesc(request.getDesc());
      goods.setGoodsClassType(request.getGoodsClassType());
      goods.setOrderNum(request.getOrderNum());
      goods.setFloorBuyNum(request.getFloorBuyNum());
      goods.setTopBuyNum(request.getTopBuyNum());
      goods.setOutOfSale("1"); //新增商品 默认是下架的
      if(request.getGoodsImgs() != null && !"".equals(request.getGoodsImgs())) {
        goods.setGoodsImgList(Arrays.asList(request.getGoodsImgs().split(",")));
      }
      goods.setPromotion(request.getPromotion());
      goods.setStock(request.getStock());
      goods.setCreateTime((new Date()).getTime());
      goods.setUpdateTime((new Date()).getTime());

      goodsService.insertGoods(goods);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 插入商品数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新商品
   * @return
   * @throws IOException
   */
  @PostMapping("/goods/update")
  public ApiResponse updateGoods(@RequestBody GoodsRequest request) throws IOException {

    logger.info("更新商品接口参数 {},", request);

    try {
      Goods goods = new Goods();
      goods.setId(request.getId() != null? request.getId(): null);
      goods.setTitle(request.getTitle() != null? request.getTitle(): null);
      goods.setVipPrice(request.getVipPrice() != null? request.getVipPrice(): null);
      goods.setOriginPrice(request.getOriginPrice() != null? request.getOriginPrice(): null);
      goods.setTags(request.getTags() != null? request.getTags(): null);
      goods.setCalType(request.getCalType() != null? request.getCalType(): null);
      goods.setGoodsClassType(request.getGoodsClassType() != null? request.getGoodsClassType(): null);
      goods.setThumb(request.getThumb() != null? request.getThumb(): null);
      goods.setDesc(request.getDesc() != null? request.getDesc(): null);
      goods.setOrderNum(request.getOrderNum() != null? request.getOrderNum(): null);
      goods.setFloorBuyNum(request.getFloorBuyNum() != null? request.getFloorBuyNum(): null);
      goods.setTopBuyNum(request.getTopBuyNum() != null? request.getTopBuyNum(): null);
      if(request.getGoodsImgs() != null && !"".equals(request.getGoodsImgs())) {
        goods.setGoodsImgList(Arrays.asList(request.getGoodsImgs().split(",")));
      }
      goods.setPromotion(request.getPromotion());
      goods.setStock(request.getStock());
      goods.setUpdateTime((new Date()).getTime());

      goodsService.updateGoods(goods);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新商品数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 删除商品
   * @return
   * @throws IOException
   */
  @PostMapping("/goods/delete")
  public ApiResponse deleteGoods(@RequestBody GoodsRequest request) throws IOException {

    logger.info("删除商品接口参数 {},", request);

    try {
      goodsService.deleteGoods(request.getId());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 删除商品数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新商品上下架状态
   * @return
   * @throws IOException
   */
  @PostMapping("/goods/update/outOfSale")
  public ApiResponse updataGoodsStatus(@RequestBody GoodsRequest request) throws IOException {

    logger.info("更新商品状态接口参数 {},", request);

    try {
      Goods goods = new Goods();
      goods.setId(request.getId());
      goods.setOutOfSale(request.getOutOfSale());

      goodsService.updateGoodsStatus(goods);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 删除商品数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 获取订单商品列表信息
   */
  @PostMapping(value = "/goods/purchaseGoods/list")
  ApiResponse getOrdersGoodsList(@RequestBody GoodsRequest request) {
    logger.info("Request 获取订单商品列表信息, 参数：" + JSONObject.toJSONString(request));

    try {

      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("area", (request.getAreaId() != null && !"".equals(request.getAreaId())) ? request.getAreaId() : null);
      paramMap.put("distributionStation", (request.getDsId() != null && !"".equals(request.getDsId())) ? request.getDsId() : null);
      paramMap.put("goodsClassType", request.getGoodsClassType() != null ? request.getGoodsClassType() : null);
      paramMap.put("currentPage", (request.getCurrentPage() - 1) * request.getPageSize());
      paramMap.put("pageSize", request.getPageSize());

      List<OrdersGoods> goodsList = goodsService.getPurchaseGoodsByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("list", goodsList);
      Integer totalNum = goodsService.getPurchaseGoodsNumByConditions(paramMap);
      data.put("total",  totalNum != null ?  totalNum : 0);

      //查询字典
      List<Dictionary> calTypeList = commonToolsService.getDictionaryListByType("calType");
      List<Dictionary> goodsClassList = commonToolsService.getDictionaryListByType("goodsClass");
      data.put("calType", calTypeList);
      data.put("goodsClass", goodsClassList);


      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取订单商品列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新采购商品数据
   */
  @PostMapping(value = "/goods/purchaseGoods/update")
  ApiResponse updatePurchaseGoodsPrice(@RequestBody GoodsRequest request) {
    logger.info("Request 更新采购商品数据, 参数：" + JSONObject.toJSONString(request));

    try {
      OrdersGoods og = new OrdersGoods();
      og.setPurchaseGoodsPriceId(request.getPurchaseGoodsPriceId());
      og.setId(request.getId());
      //设置采购价格更新
      og.setWholesalePrice(request.getWholesalePrice());
      og.setVipPrice(request.getVipPrice());

      //设置采购数量的更新
      og.setPurchaseGoodsId(request.getPurchaseGoodsId());
      og.setFirstPurchaseNum(request.getFirstPurchaseNum());
      og.setSecondPurchaseNum(request.getSecondPurchaseNum());
      og.setReturnGoodsNum(request.getReturnGoodsNum());

      goodsService.updatePurchaseGoods(og);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新采购商品价格数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 发布商品价格
   */
  @PostMapping(value = "/goods/publishGoodsPrice")
  ApiResponse publishGoodsPrice(@RequestBody GoodsRequest request) {
    logger.info("Request 发布商品价格, 参数：" + JSONObject.toJSONString(request));

    try {

      goodsService.publishGoodsPrice(request.getPurchaseDate());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 发布商品价格异常，" + e.getMessage());
      return ApiResponse.error(e.getMessage());
    }
  }

  /**
   * 再次促销
   */
  @PostMapping(value = "/goods/promotionAgain")
  ApiResponse promotionGoodsAgain(@RequestBody GoodsRequest request) {
    logger.info("Request 再次促销, 参数：" + JSONObject.toJSONString(request));

    try {
      goodsService.promotionGoodsAgain(request.getId());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 再次促销异常，" + e.getMessage());
      return ApiResponse.error(e.getMessage());
    }
  }

}