package com.tencent.wxcloudrun.controller.login;

import com.tencent.wxcloudrun.util.TokenUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
    // 这里我们也可以放在redis


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 如果我们的map有浏览器存过来的sessionid我们就任务他是已经登录了。
        String token = request.getHeader("Authorization");
        if(TokenUtils.verify(token)){
            return true;
        }
        // 失败我们跳转新的页面
        request.setAttribute("msg","登录出错");
        request.getRemoteHost();
        request.getRequestDispatcher("/login").forward(request,response);
        return false;
    }

}
