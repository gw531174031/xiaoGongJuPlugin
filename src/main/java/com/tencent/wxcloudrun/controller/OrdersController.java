package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.dto.OrdersRequest;
import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.Operation;
import com.tencent.wxcloudrun.model.Orders;
import com.tencent.wxcloudrun.service.CommonToolsService;
import com.tencent.wxcloudrun.service.OrdersService;
import com.tencent.wxcloudrun.util.TokenUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;


/**
 * counter控制器
 */
@RestController
public class OrdersController {

  final OrdersService ordersService;

  final CommonToolsService commonToolsService;
  final Logger logger;

  public OrdersController(OrdersService ordersService, CommonToolsService commonToolsService) {
    this.ordersService = ordersService;
    this.commonToolsService = commonToolsService;
    this.logger = LoggerFactory.getLogger(OrdersController.class);
  }


  /**
   * 根据手机号 状态获取订单列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/getOrderList")
  ApiResponse getOrderList(@RequestBody OrdersRequest request) {
    logger.info("Request 根据手机号，状态获取订单列表信息，参数" +  JSONObject.toJSONString(request));

    try {

      List<Orders> ordersList = ordersService.getOrdersPageByTel(request.getStatus(), request.getTel(),
              request.getCurrentPage(), request.getPageSize());
      return ApiResponse.ok(ordersList);
    } catch (Exception e) {
      logger.error("controller 异常 根据手机号，状态获取订单列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据手机号 状态获取订单列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/insertOrder")
  ApiResponse insertOrder(@RequestBody OrdersRequest request) {
    logger.info("Request 插入订单信息, 参数：" + JSONObject.toJSONString(request));

    try {
      if(request.getGoodsList() == null || request.getGoodsList().isEmpty()) {
        return ApiResponse.error("参数错误，订单商品为空");
      }

      Long nowTime = (new Date()).getTime();
      Orders orders = new Orders();
      //订单id生成的规则  3位随机数 + 手机号码中间5位 + 时间戳 毫秒
      String randomNumeric = RandomStringUtils.randomNumeric(3);
      String telString = request.getTel().substring(3, 8);
      orders.setId(randomNumeric + telString + nowTime);
      orders.setTel(request.getTel());
      orders.setReceiverId(request.getReceiverId());
      orders.setRemark(request.getRemark());
      orders.setStatus(request.getStatus());
      orders.setCreateTime(nowTime);
      orders.setUpdateTime(nowTime);
      //填充商品信息
      List<Goods> goodsListTmp = new ArrayList<>();
//      for (Goods g: request.getGoodsList()) {
//        g.setOrderId(orders.getId());
//        goodsListTmp.add(g);
//      }
//      orders.setGoodsList(goodsListTmp);
      ordersService.insertOrders(orders);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 插入订单信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据手机号 状态获取订单列表信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/updateOrder")
  ApiResponse updateOrder(@RequestBody OrdersRequest request) {
    logger.info("Request 更新订单信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Long nowTime = (new Date()).getTime();
      Orders orders = new Orders();
      orders.setId(request.getId()); //使用uuid生成id
      orders.setStatus(request.getStatus());
      orders.setUpdateTime(nowTime);

      ordersService.updateOrdersStatus(orders);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新订单列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据条件获取订单列表信息
   */
  @PostMapping(value = "/order/list")
  ApiResponse getOrderListByConditions(@RequestBody OrdersRequest request) {
    logger.info("Request 根据条件获取订单列表信息, 参数：" + JSONObject.toJSONString(request));

    try {

      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("orderId", request.getId() != null ? request.getId() : null);
      paramMap.put("keyWord", request.getKeyWord() != null ? request.getKeyWord() : null);
      paramMap.put("areaId", request.getAreaId() != null ? request.getAreaId() : null);
      paramMap.put("dsId", request.getDsId() != null ? request.getDsId() : null);
      paramMap.put("estateId", request.getEstateId() != null ? request.getEstateId() : null);
      paramMap.put("orderStatus", request.getStatus() != null ? request.getStatus() : null);
      paramMap.put("receiveTime", request.getReceiveTime() != null ? request.getReceiveTime() : null);

      if(request.getQueryTime() != null && request.getQueryTime() != 0) {
        paramMap.put("startTime", request.getQueryTime());
        paramMap.put("endTime", request.getQueryTime() + 24 * 60 * 60 * 1000);
      }
      paramMap.put("currentPage", (request.getCurrentPage() - 1) * request.getPageSize());
      paramMap.put("pageSize", request.getPageSize());

      List<Orders> ordersList = ordersService.getOrderByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("list", ordersList);
      Integer totalNum = ordersService.getOrderCountByConditions(paramMap);
      data.put("total",  totalNum != null ?  totalNum : 0);

      //查询字典
      List<Dictionary> orderStatusList = commonToolsService.getDictionaryListByType("orderStatus");
      data.put("orderStatus", orderStatusList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据条件获取订单列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据id 获取订单信息
   */
  @PostMapping(value = "/order/detail")
  ApiResponse getOrderById(@RequestBody OrdersRequest request) {
    logger.info("Request 根据id 获取订单信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Orders order= ordersService.getOrderById(request.getId());
      Map<String, Object> data = new HashMap<>();
      data.put("data", order);

      //查询字典
      List<Dictionary> orderStatusList = commonToolsService.getDictionaryListByType("orderStatus");
      List<Dictionary> calTypeList = commonToolsService.getDictionaryListByType("calType");
      List<Dictionary> receivingTimeList = commonToolsService.getDictionaryListByType("receiveTime");
      data.put("orderStatus", orderStatusList);
      data.put("calType", calTypeList);
      data.put("receiveTime", receivingTimeList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据id 获取订单信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 插入订单操作备注
   */
  @PostMapping(value = "/order/operation/remark")
  ApiResponse insertOrderOperationRemark(@RequestBody OrdersRequest request) {
    logger.info("Request 插入订单操作备注, 参数：" + JSONObject.toJSONString(request));

    try {
      Operation operation = new Operation();
      operation.setLinkId(request.getId());
      operation.setOperation(request.getOperation());
      operation.setCreateTime((new Date()).getTime());
      operation.setTel(request.getOperator());
      operation.setType("1");

      ordersService.insertOrderOperationRemark(operation);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 插入订单操作备注异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新商品的重量录入数据  分拣录入数据
   */
  @PostMapping(value = "/order/update/orderGoodsNum")
  ApiResponse updateOrderGoodsNum(@RequestBody OrdersRequest request) {
    logger.info("Request 更新商品的重量录入数据, 参数：" + JSONObject.toJSONString(request));

    try {
      Orders orders = new Orders();
      orders.setId(request.getId());
      orders.setGoodsList(request.getGoodsList());
      orders.setStatus("2");//更新订单状态 为已分拣
      orders.setUpdateTime((new Date()).getTime());

      Operation operation = new Operation();
      operation.setLinkId(request.getId());
      operation.setOperation(request.getOperation());
      operation.setCreateTime((new Date()).getTime());
      operation.setTel(request.getOperator());
      operation.setType("1");

      ordersService.updateOrdersGoodsNum(orders, operation);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新商品的重量录入数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 配送商品的状态更改
   */
  @PostMapping(value = "/order/update/deliverStatus")
  ApiResponse updateOrderStatusToDeliver(@RequestBody OrdersRequest request, HttpServletRequest hr) {
    logger.info("Request 配送商品的状态更改, 参数：" + JSONObject.toJSONString(request));

    try {
      Orders orders = new Orders();
      orders.setId(request.getId());
      orders.setStatus(request.getStatus());//更新订单状态 为已分拣
      orders.setUpdateTime((new Date()).getTime());

      String token = hr.getHeader("Authorization");
      String username = TokenUtils.getUsername(token);

      ordersService.updateOrderStatusToDeliver(orders, username);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 配送商品的状态更改异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }




}