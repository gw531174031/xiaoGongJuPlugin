package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.*;

import java.util.List;
import java.util.Map;

public interface UserService {


  User getUserCenterByTel(String tel) throws Exception;

  User getUserByTelBasic( String tel) throws Exception;

  void insertUser( User user );

  void insertStaffUser( User user );

  void updateUserBasicInfo(User user); //更新基础的信息

  void updateUserVipTime( User user );

  User getUserBasicInfoByTel(String tel) throws Exception;

  User getQRByTel(String tel) throws Exception;

  List<Role> getRoleByTel(String tel);

  List<Menus> getMenusByTel( String tel);

  List<User> getUserByConditions(Map<String, Object> paramMap);

  Integer getUserTotalNumByConditions(Map<String, Object> paramMap);

  List<Role> getRolesByConditions(String roleName, Integer currentPage, Integer pageSize);

  void updateRole(String tel, String[] roleIds);

  void updateUserStatus(String tel, String actived);

  List<CommonArea> getCommonAreaByConditions(Map<String, Object> paramMap);

  void deleteUser(String tel);

  List<CommonArea> getEstateListByArea(Map<String, Object> paramMap);

}
