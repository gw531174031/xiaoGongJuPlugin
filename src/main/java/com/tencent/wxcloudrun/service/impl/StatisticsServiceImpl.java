package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.StatisticsMapper;
import com.tencent.wxcloudrun.dto.StatisticsRequest;
import com.tencent.wxcloudrun.model.StatisticsBasic;
import com.tencent.wxcloudrun.model.StatisticsModel;
import com.tencent.wxcloudrun.service.StatisticsService;
import com.tencent.wxcloudrun.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class StatisticsServiceImpl implements StatisticsService {


  final StatisticsMapper statisticsMapper;

  final Logger logger;

  final static  Long  TOW_HOURS = 2 * 60 * 60 * 1000L; //因头一天的订单 截止日期是 凌晨两点

  final static  Long  ONE_DAY = 24 * 60 * 60 * 1000L; //一天的时间戳日期


  public StatisticsServiceImpl(StatisticsMapper statisticsMapper) {
    this.statisticsMapper = statisticsMapper;


    this.logger = LoggerFactory.getLogger(StatisticsServiceImpl.class);
  }


  /**
   * 统计基础的数据
   * @param request
   * @return
   */
  @Override
  public StatisticsBasic getStatisticsBasic(StatisticsRequest request) throws ParseException {

    try{

      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("yesterdayStartTime", DateUtils.getYestoday() + TOW_HOURS);
      paramMap.put("yesterdayEndTime", DateUtils.getYestoday() + ONE_DAY + TOW_HOURS);
      paramMap.put("areaId", request.getAreaId());
      paramMap.put("dsId", request.getDsId());

      paramMap.put("bydayStartTime", DateUtils.getYestoday() + TOW_HOURS - ONE_DAY); //前天的起始
      paramMap.put("bydayEndTime", DateUtils.getYestoday() + TOW_HOURS); //前天的截止日期

      paramMap.put("weekStartTime", DateUtils.getStartOrEndDayOfWeek(true) + TOW_HOURS);
      paramMap.put("nowTime", (new Date()).getTime());

      paramMap.put("lastWeekStartTime", DateUtils.getLastWeekStartOrEndDayOfWeek().get("beginTime") + TOW_HOURS);
      paramMap.put("lastWeekEndTime", DateUtils.getLastWeekStartOrEndDayOfWeek().get("endTime") + ONE_DAY + TOW_HOURS);

      paramMap.put("monthStartTime", DateUtils.getStartOrEndDayOfMonth(true) + TOW_HOURS);
      paramMap.put("lastMonthStartTime", DateUtils.getLastStartOrEndDayOfMonth().get("beginTime") + TOW_HOURS);
      paramMap.put("lastMonthEndTime", DateUtils.getLastStartOrEndDayOfMonth().get("endTime") + ONE_DAY + TOW_HOURS);
      StatisticsBasic sb = statisticsMapper.getOrderBasicStatistics(paramMap);

      //统计采购数据
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      String todayPurchaseDate = sdf.format(new Date());
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, -1);
      String yesterdayPurchaseDate = sdf.format( cal.getTime() );
      paramMap.put("todayPurchaseDate", todayPurchaseDate);
      paramMap.put("yesterdayPurchaseDate", yesterdayPurchaseDate);
      StatisticsBasic sb2 = statisticsMapper.getGoodsPurchaseStatistics(paramMap);
      if (sb2 != null) {
        sb.setTodayPurchaseAmount(sb2.getTodayPurchaseAmount());
        sb.setYesterdayPurchaseAmount(sb2.getYesterdayPurchaseAmount());
      }
      //统计订单状态数据
      StatisticsBasic sb3 = statisticsMapper.getOrderStatusStatistics(paramMap);
      if (sb3 != null) {
        sb.setStatusOrderedNum(sb3.getStatusOrderedNum());
        sb.setStatusSortedNum(sb3.getStatusSortedNum());
        sb.setStatusFinishNum(sb3.getStatusFinishNum());
        sb.setStatusCancelNum(sb3.getStatusCancelNum());
        sb.setStatusAfterSaleNum(sb3.getStatusAfterSaleNum());
      }

      //统计用户数据
      paramMap.put("todayStartTime", DateUtils.getYestoday() + ONE_DAY);
      StatisticsBasic sb4 = statisticsMapper.getUserStatistics(paramMap);
      if (sb4 != null) {
        sb.setCurrentUserNum(sb4.getCurrentUserNum());
        sb.setTotalVipUserNum(sb4.getTotalVipUserNum());
        sb.setTodayNewlyUserNum(sb4.getTodayNewlyUserNum());
        sb.setTodayActiveUserNum(sb4.getTodayActiveUserNum());
        sb.setYesterdayActiveUserNum(sb4.getYesterdayActiveUserNum());
        sb.setWeekActiveUserNum(sb4.getWeekActiveUserNum());
        sb.setWeekNewlyUserNum(sb4.getWeekNewlyUserNum());
        sb.setLastWeekActiveUserNum(sb4.getLastWeekActiveUserNum());
        sb.setMonthActiveUserNum(sb4.getMonthActiveUserNum());
        sb.setMonthNewlyUserNum(sb4.getMonthNewlyUserNum());
        sb.setLastMonthActiveUserNum(sb4.getLastMonthActiveUserNum());
      }

      return sb;
    } catch (Exception e ){
      logger.error("基础信息统计失败\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 统计订单数据
   * @param request
   * @return
   */
  @Override
  public List<StatisticsModel> getOrderStatistics(StatisticsRequest request) {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("startTime", request.getStatisticsStartTime());
      paramMap.put("endTime", request.getStatisticsEndTime());
      paramMap.put("areaId", request.getAreaId());
      paramMap.put("dsId", request.getDsId());
      paramMap.put("estateId", request.getEstateId());

      return statisticsMapper.getOrderStatistics(paramMap);
    }  catch (Exception e ){
      logger.error("统计订单数据失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 统计用户活跃度数据
   * @param request
   * @return
   */
  @Override
  public List<StatisticsModel> getUserStatistics(StatisticsRequest request) {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("startTime", request.getStatisticsStartTime());
      paramMap.put("endTime", request.getStatisticsEndTime());
      paramMap.put("areaId", request.getAreaId());
      paramMap.put("dsId", request.getDsId());
      paramMap.put("estateId", request.getEstateId());

      return statisticsMapper.getUserActiveStatistics(paramMap);
    }  catch (Exception e ){
      logger.error("统计用户活跃度数据失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 各商品品类销量统计
   * @param request
   * @return
   */
  @Override
  public List<StatisticsModel> getGoodsClassSaleStatistics(StatisticsRequest request) {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("goodsClassType", request.getGoodsClassType());
      paramMap.put("startTime", request.getStatisticsStartTime());
      paramMap.put("endTime", request.getStatisticsEndTime());
      paramMap.put("areaId", request.getAreaId());
      paramMap.put("dsId", request.getDsId());
      paramMap.put("estateId", request.getEstateId());

      return statisticsMapper.getGoodsClassStatistics(paramMap);
    }  catch (Exception e ){
      logger.error("各商品品类销量统计失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 单品销售统计
   * @param request
   * @return
   */
  @Override
  public List<StatisticsModel> getGoodsSaleStatistics(StatisticsRequest request) {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("startTime", request.getStatisticsStartTime());
      paramMap.put("endTime", request.getStatisticsEndTime());
      paramMap.put("areaId", request.getAreaId());
      paramMap.put("dsId", request.getDsId());
      paramMap.put("estateId", request.getEstateId());
      paramMap.put("goodsId", request.getGoodsId());

      return statisticsMapper.getGoodsStatistics(paramMap);
    }  catch (Exception e ){
      logger.error("单品销售统计失败\r\n" + e.getMessage());
      throw e;
    }
  }

}
