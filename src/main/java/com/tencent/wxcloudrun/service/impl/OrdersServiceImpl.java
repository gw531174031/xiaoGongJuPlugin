package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.GoodsMapper;
import com.tencent.wxcloudrun.dao.OperationMapper;
import com.tencent.wxcloudrun.dao.OrdersMapper;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.Operation;
import com.tencent.wxcloudrun.model.Orders;
import com.tencent.wxcloudrun.model.OrdersGoods;
import com.tencent.wxcloudrun.service.OrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@EnableTransactionManagement
@Service
public class OrdersServiceImpl implements OrdersService {


  final GoodsMapper goodsMapper;

  final OrdersMapper ordersMapper;

  final OperationMapper operationMapper;

  final Logger logger;


  public OrdersServiceImpl(GoodsMapper goodsMapper, OrdersMapper ordersMapper, OperationMapper operationMapper) {
    this.goodsMapper = goodsMapper;
    this.ordersMapper = ordersMapper;
    this.operationMapper = operationMapper;
    this.logger = LoggerFactory.getLogger(OrdersServiceImpl.class);
  }


  @Override
  public List<Orders> getOrdersPageByTel(String status, String tel, int currentPage, int pageSize)  {

    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("status", status);
    paramMap.put("tel", tel);
    paramMap.put("currentIndex", currentPage * pageSize);
    paramMap.put("pageSize", pageSize);

    try{
      List<Orders> ordersList = ordersMapper.getOrdersPageByTel(paramMap);
      if(ordersList == null || ordersList.isEmpty()) {
        return null;
      }
      List<String> orderIds = new ArrayList<>();
      for (Orders o: ordersList ) {
        orderIds.add(o.getId());
      }
      List<OrdersGoods> goodsList = goodsMapper.getGoodsByOrdersIds(orderIds);
      List<Orders> ordersListNew = new ArrayList<>();
      for(Orders o: ordersList) {
        List<Goods> goodsList1 = new ArrayList<>();
        float totalPrice = 0; // 每一单的总价
        int totalGoodsNum = 0; //每一个订单的商品总量
//        for(Goods g: goodsList) {
//          if(o.getId().equals(g.getOrderId())) { // 如果订单id匹配上了 就把数据赋值个商品list
//            goodsList1.add(g);
//            //根据每个商品在每个订单时的计价方式 统计每个订单的总价
//            if("1".equals(g.getPriceType())) {
//              totalPrice += (g.getVipPrice() * g.getGoodsNum());
//            } else if("0".equals(g.getPriceType())) {
//              totalPrice += (g.getOriginPrice() * g.getGoodsNum());
//            }
//            totalGoodsNum += g.getGoodsNum();
//          }
//        }
//        o.setOrderAmount(totalPrice);
//        o.setTotalGoodsNum(totalGoodsNum);
//        o.setGoodsList(goodsList1);
        ordersListNew.add(o);
      }
      return ordersListNew;
    } catch (Exception e) {
      logger.error("根据状态和tel查询订单列表失败\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 插入订单信息
   * @param orders
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void insertOrders(Orders orders) {

    try{
      ordersMapper.insertOrders(orders);
//      ordersMapper.insertOrdersGoods(orders.getGoodsList());
//      //删除购物车里的商品
//      for(Goods g: orders.getGoodsList()) {
//        goodsMapper.deleteCartByGoodsId(orders.getTel(), g.getId());
//      }
    }catch (Exception e){
      logger.error("插入订单信息失败！！" + e.getMessage());
//      logger.error(Arrays.toString(e.getStackTrace()));
      throw e;
    }
  }

  /**
   * 更新订单信息
   * @param orders
   */
  @Override
  public void updateOrdersStatus(Orders orders) {

    try{
      ordersMapper.updateOrdersStatus(orders);
    }catch (Exception e){
      logger.error("更新订单信息失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据条件查询订单
   * @param paramMap
   * @return
   */
  @Override
  public List<Orders> getOrderByConditions(Map<String, Object> paramMap) {


    try{
      return ordersMapper.getOrderByConditions(paramMap);
    }catch (Exception e){
      logger.error("根据条件查询订单！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据条件查询订单总数
   * @param paramMap
   * @return
   */
  @Override
  public Integer getOrderCountByConditions(Map<String, Object> paramMap) {
    try{
      return ordersMapper.getOrderCountByConditions(paramMap);
    }catch (Exception e){
      logger.error("根据条件查询订单总数失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据id查询订单详细信息
   * @param id
   * @return
   */
  @Override
  public Orders getOrderById(String id) throws Exception {

    try{
      Orders orders = ordersMapper.getOrderById(id);
      if(orders == null) {
        logger.error("查询的订单为空！");
        throw new Exception();
      }

      //查询订单的商品信息
      List<String> orderIds = new ArrayList<>();
      orderIds.add(orders.getId());
      List<OrdersGoods> ordersGoodsList = goodsMapper.getGoodsByOrdersIds(orderIds);
      orders.setGoodsList(ordersGoodsList);

      //查询订单额操作记录
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("type", "1");
      paramMap.put("linkId", orders.getId());
      List<Operation> operationList = operationMapper.getOperationByConditions(paramMap);
      orders.setOrderOperationList(operationList);

      return orders;
    }catch (Exception e){
      logger.error("根据ID查询订单详情失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 插入订单的操作备注
   * @param operation
   */
  @Override
  public void insertOrderOperationRemark(Operation operation) {

    try{
      operationMapper.insertOperation(operation);
    }catch (Exception e){
      logger.error("插入订单的操作备注失败！！\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 更新订单商品数据 商品数量
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void updateOrdersGoodsNum(Orders orders, Operation operation) {

    try{
      operationMapper.insertOperation(operation);
      for(OrdersGoods og: orders.getGoodsList()) {
        ordersMapper.updateOrdersGoods(og);
      }
      ordersMapper.updateOrdersStatus(orders);

    }catch (Exception e){
      logger.error("更新订单商品数据失败！！\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 配送商品的状态更改
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void updateOrderStatusToDeliver(Orders orders, String operator) {

    try{
      ordersMapper.updateOrdersStatus(orders);

      Operation operation = new Operation(operator, "修改订单状态为配送中",orders.getId(),
              "1", (new Date()).getTime()); //这里的type 为 1 表示订单类型的操作
      operationMapper.insertOperation(operation);
    }catch (Exception e){
      logger.error("配送商品的状态更改失败！！\r\n" + e.getMessage());
      throw e;
    }





  }

}
