package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.ReceiverMapper;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.Orders;
import com.tencent.wxcloudrun.model.Receiver;
import com.tencent.wxcloudrun.service.OrdersService;
import com.tencent.wxcloudrun.service.ReceiverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReceiverServiceImpl implements ReceiverService {


  final ReceiverMapper receiverMapper;

  final Logger logger;


  public ReceiverServiceImpl(ReceiverMapper receiverMapper) {
    this.receiverMapper = receiverMapper;

    this.logger = LoggerFactory.getLogger(ReceiverServiceImpl.class);
  }


  /**
   * 根据用户手机号查询收件人信息
   * @param tel
   * @return
   */
  @Override
  public List<Receiver> getReceiverByTel(String tel) {

    try{
      return receiverMapper.getReceiverByTel(tel);
    } catch (Exception e ){
      logger.error("根据手机号查询收件人信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 插入收件人信息
   * @param receiver
   */
  @Override
  public void insertReceiver(Receiver receiver) {

    try{
      receiverMapper.insertReceiver(receiver);
    } catch (Exception e ){
      logger.error("插入收件人信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 更新收件人信息
   * @param receiver
   */
  @Override
  public void updateReceiver(Receiver receiver) {

    try{
      //先查询是否有地址
      List<Receiver> receivers = receiverMapper.getReceiverByTel(receiver.getTel());
      Receiver receiverOld = receivers.get(0);
      receiverOld.setReceiverName(receiver.getReceiverName());
      receiverOld.setReceiverTel(receiver.getReceiverTel());
      receiverOld.setProvince(receiver.getProvince());
      receiverOld.setCity(receiver.getCity());
      receiverOld.setDistrict(receiver.getDistrict());
      receiverOld.setAddrDetail(receiver.getAddrDetail());
      receiverOld.setUpdateTime(receiver.getUpdateTime());

      receiverMapper.updateReceiver(receiverOld);
    } catch (Exception e ){
      logger.error("更新收件人信息失败\r\n" + e.getMessage());
      throw e;
    }
  }
}
