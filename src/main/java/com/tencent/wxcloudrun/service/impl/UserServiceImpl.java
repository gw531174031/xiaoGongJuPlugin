package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dao.OrdersMapper;
import com.tencent.wxcloudrun.dao.ReceiverMapper;
import com.tencent.wxcloudrun.dao.UserMapper;
import com.tencent.wxcloudrun.model.*;
import com.tencent.wxcloudrun.service.UserService;
import com.tencent.wxcloudrun.util.MD5Util;
import com.tencent.wxcloudrun.util.QRCodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {


  final UserMapper userMapper;

  final OrdersMapper ordersMapper;

  final ReceiverMapper receiverMapper;

  final Logger logger;

  @Value("${pic.dir}")
  private String picDir;

  private final static String  LOGO_IMG_NAME = "logo/132.jpeg";

  private final static String HTTP_PREFIX = "https://127.0.0.1/pic/file/user_QR_img/";

  public UserServiceImpl(UserMapper userMapper, OrdersMapper ordersMapper, ReceiverMapper receiverMapper) {
    this.userMapper = userMapper;
    this.ordersMapper = ordersMapper;
    this.receiverMapper = receiverMapper;
    this.logger = LoggerFactory.getLogger(UserServiceImpl.class);
  }


  /**
   * 根据用户手机号查询用户信息
   * @param tel
   * @return
   */
  @Override
  public User getUserCenterByTel(String tel) throws Exception {

    try{
      User user = userMapper.getUserByTel(tel);

      //统计用户各个订单状态的数据，主要是已下单 配送中
      List<OrdersStatis> ordersStatisList = ordersMapper.countOrdersByTel(tel);

      //统计用户会员周期内的 消费数据
      List<OrdersStatis> totalPriceStatis = ordersMapper.countTotalPriceByTel(tel, user.getVipStartTime(), user.getVipEndTime());
      ordersStatisList.addAll(totalPriceStatis);
      user.setOrdersStatisList(ordersStatisList);

      //自动实时的生成用户会员二维码图片 并返回访问地址
      String oldQRFileName = user.getQRFileName();
      String QRText = MD5Util.convertMD5(tel + "-" + (new Date()).getTime()); //使用md5进行加密
      //生成QR
      String QRFile = QRCodeUtils.encode(QRText, picDir + LOGO_IMG_NAME, picDir + "user_QR_img/", true);
      if (!"".equals(QRFile)) {
        user.setQRFileName(QRFile);
        user.setQrUrl(HTTP_PREFIX + QRFile);
        userMapper.updateUserForQR(user);
      }
      if(user.getQRFileName() != null && !"".equals(user.getQRFileName())) {
        File oldQRFile = new File(picDir + "user_QR_img/" + oldQRFileName);
        if (!oldQRFile.delete()) {
          logger.error("删除旧的QR失败，tel " + user.getTel() + " fileName " + oldQRFileName); //如果删除文件失败 则直接返回抛异常
        }
      }

      //处理用户是否为vip的标志 如果会员结束时间 大于等于 当前时间 则是会员 1
      user.setIsVip(user.getVipEndTime() >= (new Date()).getTime() ? "1" : "0");
      return user;
    } catch (Exception e ){
      logger.error("根据手机号查询用户信息失败" + e.getMessage());
      throw e;
    }
  }

  @Override
  public User getUserByTelBasic(String tel) throws Exception {

    try{
      return userMapper.getUserByTel(tel);
    } catch (Exception e ){
      logger.error("根据手机号查询用户信息失败" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据用户手机号查询用户基础信息
   * @param tel
   * @return
   */
  @Override
  public User getUserBasicInfoByTel(String tel) throws Exception {

    try{
      User user = userMapper.getUserByTel(tel);
      //统计用户会员周期内的 消费数据
      List<OrdersStatis> totalPriceStatis = ordersMapper.countTotalPriceByTel(tel, user.getVipStartTime(), user.getVipEndTime());
      user.setOrdersStatisList(totalPriceStatis);

      //处理用户是否为vip的标志 如果会员结束时间 大于等于 当前时间 则是会员 1
      user.setIsVip(user.getVipEndTime() >= (new Date()).getTime() ? "1" : "0");







      return user;
    } catch (Exception e ){
      logger.error("根据手机号查询用户基础信信息失败" + e.getMessage());
      throw e;
    }
  }



  /**
   * 获取用户的二维码图片
   * @param tel
   * @return
   */
  @Override
  public User getQRByTel(String tel) throws Exception {

    String QRFile;
    try{
      User user = userMapper.getUserByTel(tel);
      String oldQRFileName = user.getQRFileName();
      String QRText = MD5Util.convertMD5( tel + "-" + (new Date()).getTime() ); //使用md5进行加密
      //生成QR
      QRFile = QRCodeUtils.encode(QRText, picDir + LOGO_IMG_NAME, picDir, true);
      if (!"".equals(QRFile)) {
        user.setQRFileName(QRFile);
        user.setQrUrl(HTTP_PREFIX + QRFile);
        userMapper.updateUserForQR(user);
      }
      if(user.getQRFileName() != null && !"".equals(user.getQRFileName())) {
        File oldQRFile = new File(picDir + oldQRFileName);
        if( !oldQRFile.delete() ) {
          logger.error("删除旧的QR失败，tel " + user.getTel() + " fileName " + oldQRFileName); //如果删除文件失败 则直接返回抛异常
        }
      }
      return user;
    } catch (Exception e ){
      logger.error("根据手机号查询用户信息失败" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据手机号返回用户的角色列表
   * @param tel
   * @return
   */
  @Override
  public List<Role> getRoleByTel(String tel) {
    Map<String, Object> roleParamMap = new HashMap<>();
    roleParamMap.put("tel", tel);
    return userMapper.getRolesByConditions(roleParamMap);
  }

  /**
   * 根据用户的手机号 返回用户菜单
   * @param tel
   * @return
   */
  @Override
  public List<Menus> getMenusByTel(String tel) {
    return userMapper.getMenusByTel(tel);
  }

  /**
   * 根据条件查询用户
   * @return
   */
  @Override
  public List<User> getUserByConditions(Map<String, Object> paramMap) {

    try{
      List<User> temList = userMapper.getUserByConditions(paramMap);
      if(temList == null || temList.isEmpty()) {
        return null;
      }
      List<User> userList = new ArrayList<>();
      //循环查一页的数据
      Map<String, Object> roleParamMap = new HashMap<>();
      for(User u: temList) {
        roleParamMap.put("tel", u.getTel());
        u.setRoleList(userMapper.getRolesByConditions(roleParamMap));
        userList.add(u);
        roleParamMap.clear();
      }
      return userList;
    } catch (Exception e) {
      logger.error("根据条件查询用户列表失败\r\n" + e.getMessage());
      throw e;
    }
  }

  @Override
  public Integer getUserTotalNumByConditions(Map<String, Object> paramMap) {
    try{
      return userMapper.getUserTotalNumByConditions(paramMap);
    } catch (Exception e) {
      logger.error("根据条件查询用户总数失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据条件查询角色信息
   * @param roleName
   * @param currentPage
   * @param pageSize
   * @return
   */
  @Override
  public List<Role> getRolesByConditions(String roleName, Integer currentPage, Integer pageSize) {

    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("roleName", roleName);
    if(currentPage != null && pageSize != null) {
      paramMap.put("currentIndex", (currentPage-1) * pageSize); //由于前端传去的参数为1开始 所以这里需要减一
      paramMap.put("pageSize", pageSize);
    }

    try{
      return  userMapper.getRolesByConditions(paramMap);
    } catch (Exception e) {
      logger.error("根据条件查询角色列表失败\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 更新用户权限
   * @param tel
   * @param roleIds
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void updateRole(String tel, String[] roleIds) {
    try{
      userMapper.deleteRoleUserByTel(tel);
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("tel", tel);
      paramMap.put("list", roleIds);
      userMapper.patchInsertRoleUser(paramMap);

    } catch (Exception e) {
      logger.error(" 更新用户权限失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 更新用户的状态
   * @param tel
   * @param actived
   */
  @Override
  public void updateUserStatus(String tel, String actived) {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("tel", tel);
      paramMap.put("actived", actived);
      userMapper.updateUserStatus(paramMap);

    } catch (Exception e) {
      logger.error(" 更新用户状态失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据区域类型查询区域数据 2： 区域 3： 配送站 4： 小区
   * @return
   */
  @Override
  public List<CommonArea> getCommonAreaByConditions(Map<String, Object> paramMap) {

    try{
      return receiverMapper.getAreaByConditions(paramMap);

    } catch (Exception e) {
      logger.error(" 根据区域类型查询区域数据 失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 逻辑删除用户信息
   * @param tel
   */
  @Override
  public void deleteUser(String tel) {

    try{
      User userOld = userMapper.getUserByTel(tel);
      if(userOld == null){
        logger.error("删除账号信息失败, 用户数据为空\r\n");
        throw new Exception();
      }
      userOld.setDeleted("1");
      userOld.setUpdateTime((new Date()).getTime());

      userMapper.updateUserBasicInfo(userOld);
    } catch (Exception e ){
      logger.error("删除账号信息失败\r\n" + e.getMessage());
    }

  }

  /**
   * 通过区域信息获取小区列表
   * @param paramMap
   * @return
   */
  @Override
  public List<CommonArea> getEstateListByArea(Map<String, Object> paramMap) {
    try{
      return receiverMapper.getEstateListByArea(paramMap);
    } catch (Exception e ){
      logger.error("通过区域信息获取小区列表失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 插入用户信息
   * @param user
   */
  @Override
  public void insertUser(User user) {

    try{

      //先去查询该用户 是否已经存在，如果存在直接返回
      User userOld = userMapper.getUserByTel(user.getTel());
      if(userOld == null || userOld.getTel() == null) {
        userMapper.insertUser(user);
      }
    } catch (Exception e ){
      logger.error("插入用户信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 插入员工账号信息
   * @param user
   */
  @Override
  public void insertStaffUser(User user) {
    try{
      userMapper.insertUser(user);
    } catch (Exception e ){
      logger.error("插入员工账号信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 更新基础的用户信息
   * @param user
   */
  @Override
  public void updateUserBasicInfo(User user) {

    try{
      User userOld = userMapper.getUserByTel(user.getTel());
      if(userOld == null){
        logger.error("插入员工账号信息失败, 用户数据为空\r\n");
        throw new Exception();
      }
      userOld.setNickname(isStringNotEmpty(user.getNickname()) ? user.getNickname() : userOld.getNickname() );
      userOld.setUpdateTime((new Date()).getTime());
      userOld.setArea(isStringNotEmpty(user.getArea())  ? user.getArea() : userOld.getArea());
      userOld.setDistributionStation(isStringNotEmpty(user.getDistributionStation()) ? user.getDistributionStation() : userOld.getDistributionStation());
      userOld.setEstate( isStringNotEmpty(user.getEstate()) ? user.getEstate() : userOld.getEstate());
      userOld.setActived( isStringNotEmpty(user.getActived()) ? user.getActived() : userOld.getActived() );
      userOld.setPassword( isStringNotEmpty( user.getPassword()) ? user.getPassword() : userOld.getPassword());
      userOld.setRemark( isStringNotEmpty(user.getRemark()) ? user.getRemark() : userOld.getRemark());

      userMapper.updateUserBasicInfo(userOld);
    } catch (Exception e ){
      logger.error("更新基础的用户信息\r\n" + e.getMessage());
    }

  }

  //判空工具类
  private Boolean isStringNotEmpty(String obj){
    return obj != null && !"".equals(obj);
  }

  /**
   * 更新用户信息
   * @param user
   */
  @Override
  public void updateUserVipTime(User user) {

    try{
      userMapper.updateUserVipTime(user);
    } catch (Exception e ){
      logger.error("更新用户信息失败\r\n" + e.getMessage());
      throw e;
    }
  }


}
