package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.CommonArea;
import com.tencent.wxcloudrun.model.Orders;
import com.tencent.wxcloudrun.model.Receiver;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ReceiverService {


  List<Receiver> getReceiverByTel( String tel);

  void insertReceiver(Receiver receiver );

  void  updateReceiver(Receiver receiver );


}
