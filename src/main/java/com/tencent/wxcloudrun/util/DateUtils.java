package com.tencent.wxcloudrun.util;

import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;

/**
 * @name 时间工具类
 * @Version 1.0
 */
public class DateUtils {

    final static Logger logger = LoggerFactory.getLogger("com.tencent.wxcloudrun.util.DateUtils");

    /**
     * 获取昨天
     * @return String
     */
    public static Long getYestoday()  {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date time = cal.getTime();
        try{
           return sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(time)).getTime();
        } catch (Exception e){
            logger.error("获取昨天的日期时间戳异常！");
            return 0L;
        }
    }

    /**
     * @Description TODO 获取本周的第一天或最后一天
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     * @return: java.lang.String
     */
    public static Long getStartOrEndDayOfWeek(Boolean isFirst) {
        Date time;
        Calendar cal = Calendar.getInstance(Locale.CHINA);
//        if (isFirst) {
//            cal.add(Calendar.WEEK_OF_MONTH, 0);
//            cal.set(Calendar.DAY_OF_WEEK, 2);
//        } else {
//            cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
//            cal.add(Calendar.DAY_OF_WEEK, 1);
//        }
        cal.setTime(new Date());
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK, 2);
        time = cal.getTime();
        try{
            System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(time));
            return sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(time)).getTime();
        } catch (Exception e){
            logger.error("获取本周的第一天的日期时间戳异常！");
            return 0L;
        }
    }

    /**
     * 功能描述: TODO 获取上一周的开始时间和结束时间
     */
    public static Map<String, Long> getLastWeekStartOrEndDayOfWeek() {
        try{
            Map<String, Long> dayMap = new HashMap<>(16);
            Long beginTime;
            Long endTime;
            Calendar date = Calendar.getInstance(Locale.CHINA);
            date.setFirstDayOfWeek(Calendar.MONDAY);
            //周数减一，即上周
            date.add(Calendar.WEEK_OF_MONTH, -1);
            //日子设为周几
            date.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            beginTime = sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(date.getTime())).getTime();
            date.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            endTime = sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(date.getTime())).getTime();
            dayMap.put("beginTime", beginTime);
            dayMap.put("endTime", endTime);
            return dayMap;
        } catch (Exception e){
            logger.error("获取上一周的第一天和最后一天的日期时间戳异常！");
            return null;
        }
    }

    /**
     * 功能描述: 获取本月的第一天或最后一天
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     * @return: java.lang.String
     */
    public static Long getStartOrEndDayOfMonth(Boolean isFirst) throws ParseException {
        LocalDate today = LocalDate.now();
        LocalDate resDate = today;

        Month month = today.getMonth();
        int length = month.length(today.isLeapYear());
        if (isFirst) {
            resDate = LocalDate.of(today.getYear(), month, 1);
        } else {
            resDate = LocalDate.of(today.getYear(), month, length);
        }

        try{
            return sdf.parse(resDate.toString()).getTime();
        } catch (Exception e){
            logger.error("获取本月的第一天的日期时间戳异常！");
            return 0L;
        }
    }

    /**
     * 功能描述: 获取上月开始日期和结束日期
     */
    public static Map<String, Long> getLastStartOrEndDayOfMonth() {
        try{
            Map<String, Long> dayMap = new HashMap<>(16);
            //获取当前日期
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -1);
            //设置为1号,当前日期既为本月第一天
            cal.set(Calendar.DAY_OF_MONTH, 1);
            Long firstDay = sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime())).getTime();
            //获取前月的最后一天
            Calendar cale = Calendar.getInstance();
            //设置为1号,当前日期既为本月第一天
            cale.set(Calendar.DAY_OF_MONTH, 0);
            Long lastDay = sdf.parse(new SimpleDateFormat("yyyy-MM-dd").format(cale.getTime())).getTime();
            dayMap.put("beginTime", firstDay);
            dayMap.put("endTime", lastDay);
            return dayMap;
        } catch (Exception e){
            logger.error("获取上一月的第一天和最后一天的日期时间戳异常！");
            return null;
        }
    }

    /**
     * @Description TODO 获取本季度的第一天或最后一天
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     * @return: java.lang.String
     */
    public static String getStartOrEndDayOfQuarter(LocalDate today, Boolean isFirst) {
        LocalDate resDate = LocalDate.now();
        if (today == null) {
            today = resDate;
        }
        Month month = today.getMonth();
        Month firstMonthOfQuarter = month.firstMonthOfQuarter();
        Month endMonthOfQuarter = Month.of(firstMonthOfQuarter.getValue() + 2);
        if (isFirst) {
            resDate = LocalDate.of(today.getYear(), firstMonthOfQuarter, 1);
        } else {
            resDate = LocalDate.of(today.getYear(), endMonthOfQuarter, endMonthOfQuarter.length(today.isLeapYear()));
        }
        return resDate.toString();
    }

    /**
     * 功能描述: TODO 获取上个季度开始和结束时间
     */
    public static Map<String, String> getLastStartOrEndDayOfQuarter(){
        Map<String, String> dayMap = new HashMap<>(16);
        String beginTime = "";
        String endTime = "";
        LocalDate today = LocalDate.now();
        Month month = today.getMonth();
        // 当前季度的第一个月
        Month firstMonthOfQuarter = month.firstMonthOfQuarter();
        // 如果是当前年的第一个季度
        if(firstMonthOfQuarter.getValue() == 1){
            // 则获取去年的最后一个季度
            LocalDate lastQuarterToday = LocalDate.of(today.getYear()-1, today.getMonth().getValue()+9, today.getDayOfMonth());
            Month lastFirstMonthOfQuarter = lastQuarterToday.getMonth().firstMonthOfQuarter();
            Month endMonthOfQuarter = Month.of(lastFirstMonthOfQuarter.getValue() + 2);
            beginTime = LocalDate.of(today.getYear()-1, lastFirstMonthOfQuarter,1).toString();
            endTime = LocalDate.of(today.getYear()-1, endMonthOfQuarter, endMonthOfQuarter.length(today.isLeapYear())).toString();
        }else{
            // 则获取去年的最后一个季度
            LocalDate lastQuarterToday = LocalDate.of(today.getYear(), today.getMonth().getValue()-3, today.getDayOfMonth());
            Month lastFirstMonthOfQuarter = lastQuarterToday.getMonth().firstMonthOfQuarter();
            Month endMonthOfQuarter = Month.of(lastFirstMonthOfQuarter.getValue() + 2);
            beginTime = LocalDate.of(today.getYear(), lastFirstMonthOfQuarter,1).toString();
            endTime = LocalDate.of(today.getYear(), endMonthOfQuarter, endMonthOfQuarter.length(today.isLeapYear())).toString();
        }
        dayMap.put("beginTime", beginTime);
        dayMap.put("endTime", endTime);
        return dayMap;
    }

    /**
     * @Description TODO 获取本年的第一天或最后一天
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     * @return: java.lang.String
     */
    public static String getStartOrEndDayOfYear(LocalDate today, Boolean isFirst) {
        LocalDate resDate = LocalDate.now();
        if (today == null) {
            today = resDate;
        }
        if (isFirst) {
            resDate = LocalDate.of(today.getYear(), Month.JANUARY, 1);
        } else {
            resDate = LocalDate.of(today.getYear(), Month.DECEMBER, Month.DECEMBER.length(today.isLeapYear()));
        }
        return resDate.toString();
    }

    /**
     * @Description TODO 获取去年的第一天或最后一天
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     * @return: java.lang.String
     */
    public static Map<String, String> getLastStartOrEndDayOfYear() {
        Map<String, String> map = new HashMap<>(16);
        LocalDate today = LocalDate.now();
        LocalDate beginTime = LocalDate.of(today.getYear()-1, Month.JANUARY, 1);
        LocalDate endTime = LocalDate.of(today.getYear()-1, Month.DECEMBER, Month.DECEMBER.length(today.isLeapYear()));
        map.put("beginTime", beginTime.toString());
        map.put("endTime", endTime.toString());
        return map;
    }

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static final int DAY = 1;       //日
    public static final int WEEK = 2;      //周
    public static final int MONTH = 3;     //月
    public static final int QUARTER = 4; //季度


    public static String getWeekOfYear(String dateStr, int mode){
        Calendar calendar = Calendar.getInstance();
        //将两个日期加载到Calendar中
        if (calendar != null ) {
            try {
                Date date = sdf.parse(dateStr);
                calendar.setTime(date);
                calendar.setFirstDayOfWeek(mode);//设置周五为每周第一天Calendar.FRIDAY
                if (calendar.get(Calendar.WEEK_OF_YEAR) > 1)
                    calendar.setMinimalDaysInFirstWeek(1);//设置跨年的周超5天算本年的。
                return calendar.get(Calendar.WEEK_OF_YEAR)+"";
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    /**
     * 遍历两个日期之间的日、周、月
     * @param startDateStr 开始日期字符串
     * @param endDateStr   结束字符串
     * @param Flag         遍历的标志，按日还是按周遍历    DAY:日  WEEK：周  MONTH：月
     */
    public static String[] dateForEach(String startDateStr, String endDateStr, Integer Flag) {
        List<String> result = new ArrayList<>(12);
        //将字符串类型的日期转换为Date类型并加载到Calendar中(有异常直接抛)
        Date startDate = null;
        try {
            System.out.println("startDateStr==>" + startDateStr);
            startDate = sdf.parse(startDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            System.out.println("endDateStr==>" + endDateStr);
            endDate = sdf.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //实例化Calendar
        Calendar startCalendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        //将两个日期加载到Calendar中
        if (startDate != null && endDate != null) {
            startCalendar.setTime(startDate);
            endCalendar.setTime(endDate);
        }
        //开始遍历
        switch (Flag) {
            case WEEK:
                //按周输出
                do {

                    System.out.println(startCalendar.get(Calendar.WEEK_OF_YEAR) + "周");
                    System.out.println(startCalendar.getTime());
                    startCalendar.setFirstDayOfWeek(Calendar.FRIDAY);//设置周五为每周第一天
                    if (startCalendar.get(Calendar.WEEK_OF_YEAR) > 1)
                        startCalendar.setMinimalDaysInFirstWeek(1);//设置跨年的周超5天算本年的。
                    result.add(startCalendar.get(Calendar.WEEK_OF_YEAR) + "周");
                    startCalendar.add(Calendar.WEEK_OF_YEAR, 1);
                } while (!startCalendar.equals(endCalendar) && !startCalendar.after(endCalendar));
                break;
            case MONTH:
                do {
                    System.out.println(startCalendar.get(Calendar.MONTH) + 1 + "月");
                    result.add(startCalendar.get(Calendar.MONTH) + 1 + "月");
                    //日期加1
                    startCalendar.add(Calendar.MONTH, 1);
                    //当前日期和结束日历日期比较，超过结束日期则终止
                } while (!startCalendar.after(endCalendar));
                break;
            case QUARTER:
                do {
                    int q = 0;
                    //和按日输出差不多
                    if (startCalendar.get(Calendar.MONTH) <= 2) {
                        q = 1;
                    } else if (startCalendar.get(Calendar.MONTH) >= 3 && startCalendar.get(Calendar.MONTH) <= 5) {
                        q = 2;
                    } else if (startCalendar.get(Calendar.MONTH) >= 6 && startCalendar.get(Calendar.MONTH) <= 8) {
                        q = 3;
                    } else if (startCalendar.get(Calendar.MONTH) >= 9 && startCalendar.get(Calendar.MONTH) <= 11) {
                        q = 4;
                    }
                    System.out.println(q);
                    result.add(q + "季度");
                    //日期加1
                    startCalendar.add(Calendar.MONTH, 3);
                    //当前日期和结束日历日期比较，超过结束日期则终止
                } while (!startCalendar.after(endCalendar));
        }
        return result.toArray(new String[0]);
    }

    //测试
    public static void main(String[] args) {

        Long yesterday = getYestoday();
        Long startWeek = getStartOrEndDayOfWeek(true);
        Map<String, Long> lastWeek = getLastWeekStartOrEndDayOfWeek();
        Map<String, Long> lastMonth = getLastStartOrEndDayOfMonth();

        System.out.println(getLastStartOrEndDayOfQuarter());
    }
}


