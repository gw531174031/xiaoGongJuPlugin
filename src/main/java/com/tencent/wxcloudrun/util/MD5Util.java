package com.tencent.wxcloudrun.util;

public class MD5Util {


    /**
     * 可逆的的加密解密方法；两次是解密，一次是加密
     * @param inStr
     * @return
     */
    public static String convertMD5(String inStr){

        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++){
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        return s;

    }

    public static void main(String[] args) {
        String str = "18580203199-1652709225277";
        String md5EncodeStr = convertMD5(str);
        String md5DecodeStr = convertMD5(md5EncodeStr);

        System.out.println("原始字符串 " + str);
        System.out.println("加密后的字符串 " + md5EncodeStr);
        System.out.println("解密后的字符串 " + md5DecodeStr);

    }


}
